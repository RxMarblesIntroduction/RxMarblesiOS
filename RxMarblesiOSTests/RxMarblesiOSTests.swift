//
//  RxMarblesiOSTests.swift
//  RxMarblesiOSTests
//
//  Created by Alexandre Delattre on 24/09/2017.
//  Copyright © 2017 Alexd. All rights reserved.
//

import XCTest
@testable import RxMarblesiOS
import RxSwift
import RxTest

let resolution = 0.1

let cityValues = [
    "0" : "",
    "1" : "tou",
    "t" : "toulouse",
    "b" : "bordeaux",
]

let weatherValues = [
    "t" : WeatherData(city: "Toulouse", temp: 20, tempMin: 20, tempMax: 30, icon: "sunny"),
    "b": WeatherData(city: "Bordeaux", temp: 10, tempMin: 10, tempMax: 15, icon: "cloudy")
]


let stateValues = [
    "i" : LoadingState.idle,
    "l" : LoadingState.loading,
]


class RxMarblesiOSTests: XCTestCase {
    var weatherService: MockWeatherService!
    var viewModel: WeatherViewModel!
    var scheduler: TestScheduler!
    
    override func setUp() {
        super.setUp()
        scheduler = TestScheduler(initialClock: 0, resolution: resolution, simulateProcessingDelay: false)
        weatherService = MockWeatherService()
        viewModel = WeatherViewModel(api: weatherService, scheduler: scheduler)
    }
    
    func testSimple() {
        let input = scheduler.createObservable(timeline:              "-----t-------", values: cityValues)
        // debouncing                                                       -----t
        weatherService.mockFunction = scheduler.mock(values: weatherValues, errors: [:]) { (arg: String) in
            if (arg == "toulouse") {
                return                                                          "--t"
            } else { return "|" }
        }
        let expectedWeather = scheduler.parseEventsAndTimes(timeline: "------------t", values: weatherValues).first!
        let expectedState = scheduler.parseEventsAndTimes(timeline:   "i---------l-i", values: stateValues).first!
    
        input.bindTo(viewModel.city)
        let weather = scheduler.record(source: viewModel.weather)
        let state = scheduler.record(source: viewModel.state)
    
        scheduler.start()
    
        XCTAssertEqual(expectedState, state.events)
        XCTAssertEqual(expectedWeather, weather.events)
    }
    
    func test2Cities() {
        let input = scheduler.createObservable(timeline:              "0-1-t------------b----------", values: cityValues)
        // debouncing                                                      -----t       -----b
        weatherService.mockFunction = scheduler.mock(values: weatherValues, errors: [:]) { (arg: String) in
            if (arg == "toulouse") {
                return                                                         "--t"
            } else if (arg == "bordeaux") {
                return                                                                      "--b"
            } else { return "|" }
        }
        let expectedWeather = scheduler.parseEventsAndTimes(timeline: "-----------t------------b---", values: weatherValues).first!
        let expectedState = scheduler.parseEventsAndTimes(timeline:   "i--------l-i----------l-i---", values: stateValues).first!
        
        input.bindTo(viewModel.city)
        let weather = scheduler.record(source: viewModel.weather)
        let state = scheduler.record(source: viewModel.state)
        
        scheduler.start()
        
        XCTAssertEqual(expectedState, state.events)
        XCTAssertEqual(expectedWeather, weather.events)
    }
    
    func test2CitiesInterleaved() {
        let input = scheduler.createObservable(timeline:              "0-1-t-------b----------", values: cityValues)
        // debouncing                                                      -----t  -----b
        weatherService.mockFunction = scheduler.mock(values: weatherValues, errors: [:]) { (arg: String) in
            if (arg == "toulouse") {
                return                                                         "-----------t"
            } else if (arg == "bordeaux") {
                return                                                                 "--b"
            } else { return "|" }
        }
        let expectedWeather = scheduler.parseEventsAndTimes(timeline: "-------------------b---", values: weatherValues).first!
        let expectedState = scheduler.parseEventsAndTimes(timeline:   "i--------l-------l-i---", values: stateValues).first!
        
        input.bindTo(viewModel.city)
        let weather = scheduler.record(source: viewModel.weather)
        let state = scheduler.record(source: viewModel.state)
        
        scheduler.start()
        
        XCTAssertEqual(expectedState, state.events)
        XCTAssertEqual(expectedWeather, weather.events)
    }
}

class MockWeatherService : WeatherService {
    var mockFunction: ((String) -> Observable<WeatherData>)!
    
    func getWeather(city: String) -> Observable<WeatherData> { return mockFunction(city) }
}

