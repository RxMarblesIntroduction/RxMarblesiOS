//
//  Model.swift
//  RxMarblesiOS
//
//  Created by Alexandre Delattre on 24/09/2017.
//  Copyright © 2017 Alexd. All rights reserved.
//

import Foundation

struct WeatherData {
    let city: String
    let temp: Double
    let tempMin: Double
    let tempMax: Double
    let icon: String
}

extension WeatherData : Equatable {
    public static func ==(lhs: WeatherData, rhs: WeatherData) -> Bool {
        return lhs.city == rhs.city &&
            lhs.temp == rhs.temp &&
            lhs.tempMin == rhs.tempMin &&
            lhs.tempMax == rhs.tempMax &&
            lhs.icon == rhs.icon
    }
}
