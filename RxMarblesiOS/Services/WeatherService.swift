//
//  WeatherService.swift
//  HelloRxSwift
//
//  Created by Alexandre Delattre on 22/03/2017.
//  Copyright © 2017 Alexd. All rights reserved.
//

import Foundation
import RxSwift


protocol WeatherService {
    func getWeather(city: String) -> Observable<WeatherData>
}

struct ParsingError : Error {}

let backgroundScheduler = ConcurrentDispatchQueueScheduler(qos: .userInteractive)

class OpenWeatherMapService : WeatherService {
    let appid: String
    
    init(appid: String) {
        self.appid = appid
    }
    
    func getWeather(city: String) -> Observable<WeatherData> {
        let cityQ = city.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let req = URLRequest(url: URL(string: "http://api.openweathermap.org/data/2.5/weather?appid=\(appid)&units=metric&q=\(cityQ)")!)
        
        return URLSession.shared.rx.json(request: req)
            .observeOn(backgroundScheduler)
            .map { try self.parseJSON($0) }
    }
    
    
    private func parseJSON(_ body: Any) throws -> WeatherData {
        guard let body = body as? [String: Any] else { throw ParsingError() }
        guard let city = body["name"] as? String else { throw ParsingError() }
        guard let main = body["main"] as? [String: Any] else { throw ParsingError() }
        guard let temp = main["temp"] as? Double else { throw ParsingError() }
        guard let tempMin = main["temp_min"] as? Double else { throw ParsingError() }
        guard let tempMax = main["temp_max"] as? Double else { throw ParsingError() }
        guard let weather = body["weather"] as? [[String: Any]] else { throw ParsingError() }
        guard let icon = weather.first?["icon"] as? String else { throw ParsingError() }
        return WeatherData(city: city,
                           temp: temp,
                           tempMin: tempMin,
                           tempMax: tempMax,
                           icon: urlForPicto(icon))
    }
}


fileprivate func urlForPicto(_ picto: String) -> String {
    return "http://openweathermap.org/img/w/\(picto).png"
}

struct Container {
    static let weatherViewModel = WeatherViewModel(api: OpenWeatherMapService(appid: "a936c94519e94feff4f84f095dc3fe76"))
}
