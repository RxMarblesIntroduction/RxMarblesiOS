//
//  Swinject.swift
//  RxMarblesiOS
//
//  Created by Alexandre Delattre on 24/09/2017.
//  Copyright © 2017 Alexd. All rights reserved.
//

import Foundation
import SwinjectStoryboard

extension SwinjectStoryboard {
    public class func setup() {
        defaultContainer.register(WeatherService.self) { r in
            OpenWeatherMapService(appid: "a936c94519e94feff4f84f095dc3fe76")
        }
        
        defaultContainer.register(WeatherViewModel.self) { r in
            WeatherViewModel(api: r.resolve(WeatherService.self)!)
        }
        
        defaultContainer.storyboardInitCompleted(ViewController.self) { (r, c) in
            c.viewModel = r.resolve(WeatherViewModel.self)
        }
    }
}
