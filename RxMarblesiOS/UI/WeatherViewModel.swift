//
//  WeatherViewModel.swift
//  RxMarblesiOS
//
//  Created by Alexandre Delattre on 24/09/2017.
//  Copyright © 2017 Alexd. All rights reserved.
//

import Foundation
import RxSwift

enum LoadingState : Equatable {
    case idle
    case loading
    case error(e: Error)
}

func ==(lhs: LoadingState, rhs: LoadingState) -> Bool {
    switch (lhs, rhs) {
    case (.idle, .idle):
        return true
    case (.loading, .loading):
        return true
    case (.error, .error):
        return true
        
    default:
        return false
    }
}


class WeatherViewModel {
    
    let city: BehaviorSubject<String> = BehaviorSubject(value: "")
    let state: Observable<LoadingState>
    let weather: Observable<WeatherData>
    
    init(api: WeatherService, scheduler: SchedulerType = MainScheduler.instance) {
        let _state = BehaviorSubject<LoadingState>(value: .idle)
        self.state = _state
        self.weather = city
            .filter { $0.characters.count > 0 }
            .debounce(0.5, scheduler: scheduler)
            .distinctUntilChanged()
            .flatMapLatest { city in
                api.getWeather(city: city)
                    .observeOn(scheduler)
                    .do(onNext: { _ in
                        _state.onNext(.idle)
                    },onError: { err in
                        _state.onNext(.error(e: err))
                    }, onSubscribe: {
                        _state.onNext(.loading)
                    })
                    .catchError { _ in Observable.empty() }
            }
            .shareReplayLatestWhileConnected()
    }
}
