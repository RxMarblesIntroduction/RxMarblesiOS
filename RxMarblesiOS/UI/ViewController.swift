//
//  ViewController.swift
//  RxMarblesiOS
//
//  Created by Alexandre Delattre on 24/09/2017.
//  Copyright © 2017 Alexd. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var weatherLayout: UIView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    
    let disposeBag = DisposeBag()
    var viewModel: WeatherViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField.rx.text.map{ $0 ?? "" }.subscribe(viewModel.city).disposed(by: disposeBag)
        
        viewModel.state.map { $0 == .loading }
            .bind(to: activityIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
        
        viewModel.state.map { $0 != .idle }
            .bind(to: weatherLayout.rx.isHidden)
            .disposed(by: disposeBag)
        
        viewModel.weather.map { "\($0.tempMin) °C\n\($0.tempMax) °C" }
            .bind(to: tempLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.weather.map { $0.icon }.flatMapLatest { loadImage(url: $0).startWith(nil) }
            .bind(to: imageView.rx.image)
            .disposed(by: disposeBag)
        
    }
}

private func loadImage(url: String) -> Observable<UIImage?> {
    return URLSession.shared.rx.data(request: URLRequest(url: URL(string: url)!))
        .map { UIImage(data: $0) }
        .catchErrorJustReturn(nil)
}
